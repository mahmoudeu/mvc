<?php
/**
 * Created by PhpStorm.
 * User: macbookpro
 * Date: 17/03/2020
 * Time: 20:31
 */

namespace Core\DB;


use Core\Foundation\Config\Config;
use PDO;

class Builder
{
    public function __construct(Config $config)
    {
        $this->pdo = $this->createPdo($config);
    }

    public function insert(){

    }

    private function createPdo(Config $config)
    {
        $dbconfig = $config->get('db');
        $host = $dbconfig['host'];
        $dbname = $dbconfig['database_name'];
        $user = $dbconfig['user'];
        $password = 'password';
        $dsn = "mysql:dbname=$dbname;host=$host";

        try {
            $this->pdo = new PDO($dsn, $user, $password);
        } catch (PDOException $e) {
            throw new DbConnectionException($e->getMessage());
        }
    }

}
