<?php
/**
 * Created by PhpStorm.
 * User: macbookpro
 * Date: 17/03/2020
 * Time: 19:37
 */

namespace Core\Foundation\Session;


interface SessionInterface
{
    public function start();

    public function get($key);

    public function set($key, $value);
}
