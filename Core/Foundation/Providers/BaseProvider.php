<?php
/**
 * Created by PhpStorm.
 * User: macbookpro
 * Date: 16/03/2020
 * Time: 02:45
 */

namespace Core\Foundation\Providers;



use Core\Foundation\Application\Application;

class BaseProvider
{
    /**
     * @var Application
     */
    protected $application;

    public function __construct(Application $application)
    {

        $this->application = $application;
    }
}
