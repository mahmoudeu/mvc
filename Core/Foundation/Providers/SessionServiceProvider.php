<?php
/**
 * Created by PhpStorm.
 * User: macbookpro
 * Date: 17/03/2020
 * Time: 19:42
 */

namespace Core\Foundation\Providers;


use Core\Foundation\Session\Session;
use Core\Foundation\Session\SessionInterface;

class SessionServiceProvider extends BaseProvider implements ProviderInterface
{

    public function register()
    {
        $this->application->register(SessionInterface::class, new Session());
    }
}
