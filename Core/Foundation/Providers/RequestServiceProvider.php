<?php
/**
 * Created by PhpStorm.
 * User: macbookpro
 * Date: 16/03/2020
 * Time: 02:07
 */

namespace Core\Foundation\Providers;


use Core\Foundation\Request\Request;
use Core\Foundation\Request\RequestInterface;

class RequestServiceProvider extends BaseProvider implements ProviderInterface
{
    public function register()
    {
        $this->application->register(RequestInterface::class, new Request());
    }
}
