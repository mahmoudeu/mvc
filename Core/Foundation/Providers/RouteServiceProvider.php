<?php
/**
 * Created by PhpStorm.
 * User: macbookpro
 * Date: 17/03/2020
 * Time: 19:25
 */

namespace Core\Foundation\Providers;


use Core\Foundation\Route\RouteInterface;
use Core\Foundation\Route\Router;

class RouteServiceProvider extends BaseProvider implements ProviderInterface
{

    public function register()
    {
        $this->application->register(RouteInterface::class,new Router());
    }
}
