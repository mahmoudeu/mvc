<?php
/**
 * Created by PhpStorm.
 * User: macbookpro
 * Date: 16/03/2020
 * Time: 02:06
 */

namespace Core\Foundation\Providers;


interface ProviderInterface
{
    public function register();
}
