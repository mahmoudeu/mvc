<?php
/**
 * Created by PhpStorm.
 * User: macbookpro
 * Date: 17/03/2020
 * Time: 20:33
 */

namespace Core\Foundation\Providers;


use Core\DB\Builder;

class BuilderServiceProvider extends BaseProvider implements ProviderInterface
{

    public function register()
    {
        $this->application->register(Builder::class, $this->application->make(Builder::class));
    }
}
