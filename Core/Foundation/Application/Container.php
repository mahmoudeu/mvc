<?php
/**
 * Created by PhpStorm.
 * User: macbookpro
 * Date: 16/03/2020
 * Time: 00:28
 */

namespace Core\Foundation\Application;


class Container
{
    public $container = [];

    public function make(string $className)
    {
        if (array_key_exists($className, $this->container)) {
            return $this->container[$className];
        }
        $parameters = $this->getConstructorParameters($className);
        $classParameters = [];
        foreach ($parameters as $parameter) {
            $classParameters[] = $this->make($parameter);

        }
        return new $className(...$classParameters);
    }

    private function getConstructorParameters($className)
    {
        $reflection = new \ReflectionClass($className);
        $constructor = $reflection->getConstructor();
        if(is_null($constructor)){
            return [];
        }
        $method = $constructor->getParameters();
        $parameters = array_map(function (\ReflectionParameter $parameter) {
            return $parameter->getType();
        }, $method);
        return $parameters;
    }

    private function getMethodParameters($className, $methodName)
    {

        $reflection = new \ReflectionClass($className);
        $constructor = $reflection->getMethod($methodName);
        if(is_null($constructor)){
            return [];
        }
        $method = $constructor->getParameters();
        $parameters = array_map(function (\ReflectionParameter $parameter) {
            return $parameter->getType();
        }, $method);
        return $parameters;

    }

    public function register(string $className, $implementation)
    {
        $this->container[$className] = $implementation;
    }

    public function call($className, $methodName)
    {
        $parameters = $this->getMethodParameters($className, $methodName);
        $methodParameters = [];
        foreach ($parameters as $parameter) {
            $methodParameters[] = $this->make($parameter);
        }

        $class = $this->make($className);

        return $class->$methodName(...$methodParameters);
    }
}
