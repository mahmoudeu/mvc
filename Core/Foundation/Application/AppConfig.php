<?php
/**
 * Created by PhpStorm.
 * User: macbookpro
 * Date: 16/03/2020
 * Time: 01:18
 */

namespace Core\Foundation\Application;


class AppConfig
{
    /**
     * @var string
     */
    private $configPath;
    /**
     * @var string
     */
    private $appPath;

    /**
     * AppConfig constructor.
     * @param string $appPath
     * @param string $configPath
     */
    public function __construct(string $appPath, string $configPath)
    {

        $this->appPath = $appPath;
        $this->configPath = $configPath;
    }

    /**
     * @return string
     */
    public function getConfigPath()
    {
        return $this->configPath;
    }

    /**
     * @param string $configPath
     */
    public function setConfigPath($configPath)
    {
        $this->configPath = $configPath;
    }

    /**
     * @return string
     */
    public function getAppPath()
    {
        return $this->appPath;
    }

    /**
     * @param string $appPath
     */
    public function setAppPath($appPath)
    {
        $this->appPath = $appPath;
    }

}
