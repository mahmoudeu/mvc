<?php
/**
 * Created by PhpStorm.
 * User: macbookpro
 * Date: 16/03/2020
 * Time: 01:16
 */

namespace Core\Foundation\Application;


use Core\Foundation\Config\Config;
use Core\Foundation\Providers\BuilderServiceProvider;
use Core\Foundation\Providers\RequestServiceProvider;
use Core\Foundation\Providers\RouteServiceProvider;
use Core\Foundation\Providers\SessionServiceProvider;
use Core\Foundation\Request\RequestInterface;
use Core\Foundation\Route\RouteInterface;
use Core\Foundation\Session\SessionInterface;
use DirectoryIterator;

class Application extends Container
{
    /**
     * @var AppConfig
     */
    private $appConfig;

    public function __construct(AppConfig $config)
    {
        $this->appConfig = $config;
    }

    public function run()
    {
        $this->registerEssentials();
    }

    public function handleRequest()
    {
        $this->startSession();
        /**
         * @var $request RequestInterface
         */
        $request = $this->make(RequestInterface::class);
        $router = $this->loadRoutes();
        $action = $router->match($request);

        $this->call($action['controller'], $action['action']);
    }

    private function registerConfigs()
    {
        $configs = [];
        $dir = $this->appConfig->getConfigPath();
        foreach (new DirectoryIterator($this->appConfig->getConfigPath()) as $fileInfo) {
            if ($fileInfo->isDot()) continue;
            $configKey = strtolower($fileInfo->getBasename('.php'));
            $filename = $fileInfo->getFilename();
            $configs[$configKey] = require $dir . '/' . $filename;
        }

        $this->register(Config::class, new Config($configs));
    }


    private function registerEssentials()
    {
        $this->registerConfigs();
        $this->registerProviders();
    }

    private function registerProviders()
    {
        $this->registerCoreProviders();
        $this->registerConfigProviders();
    }

    private function startSession()
    {
        /**
         * @var $session SessionInterface
         */
        $session = $this->make(SessionInterface::class);
        $session->start();
    }

    private function loadRoutes(): RouteInterface
    {
        /**
         * @var $router RouteInterface
         */
        $router = $this->make(RouteInterface::class);

        $domainsDir = $this->appConfig->getAppPath() . '/App/Domains';
        foreach (new DirectoryIterator($domainsDir) as $fileInfo) {
            if (!$fileInfo->isDir()) continue;
            if ($fileInfo->isDot()) continue;
            $domain = $fileInfo->getPathname();
            $routesFolder = $domain . '/Routes/';

            foreach (new DirectoryIterator($routesFolder) as $fileInfo) {
                if ($fileInfo->isDot()) continue;
                $filename = $fileInfo->getFilename();
                require $routesFolder . '/' . $filename;
            }
        }


        return $router;
    }

    private function registerConfigProviders()
    {
        $providers = $this->make(Config::class)->get('providers');

        foreach ($providers as $provider) {
            (new $provider($this))->register();
        }
    }

    private function registerCoreProviders()
    {
        $providers =
            [
                RouteServiceProvider::class,
                RequestServiceProvider::class,
                SessionServiceProvider::class,
                BuilderServiceProvider::class
            ];

        foreach ($providers as $provider) {
            (new $provider($this))->register();
        }
    }
}
