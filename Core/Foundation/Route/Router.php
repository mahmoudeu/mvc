<?php
/**
 * Created by PhpStorm.
 * User: macbookpro
 * Date: 17/03/2020
 * Time: 18:06
 */

namespace Core\Foundation\Route;


use Core\Foundation\Request\RequestInterface;
use Core\Foundation\RouteNotFoundException;

class Router implements RouteInterface
{
    const METHOD_POST = 'POST';
    const METHOD_GET = 'GET';

    public $routes = [
        self::METHOD_POST,
        self::METHOD_GET
    ];

    public function get(
        $path,
        $controller,
        $action
    )
    {
        return $this->add(self::METHOD_GET, ...func_get_args());
    }

    public function post(
        $path,
        $controller,
        $action
    )
    {
        return $this->add(self::METHOD_POST, ...func_get_args());
    }

    private function add(
        $method,
        $path,
        $controller,
        $action
    )
    {
        $this->routes[$method][$path] =
            [
                'controller' => $controller,
                'action' => $action
            ];
    }

    public function match(RequestInterface $request)
    {

        if (empty($this->routes[$request->getMethod()][$request->getPath()])) {
            throw new RouteNotFoundException('route not found');
        }

        return $this->routes[$request->getMethod()][$request->getPath()];
    }
}
