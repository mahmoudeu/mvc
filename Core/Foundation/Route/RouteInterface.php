<?php
/**
 * Created by PhpStorm.
 * User: macbookpro
 * Date: 17/03/2020
 * Time: 19:35
 */

namespace Core\Foundation\Route;


use Core\Foundation\Request\RequestInterface;

interface RouteInterface
{
    public function get($path, $controller, $action);

    public function post($path, $controller, $action);

    public function match(RequestInterface $request);
}
