<?php
/**
 * Created by PhpStorm.
 * User: macbookpro
 * Date: 16/03/2020
 * Time: 00:47
 */

namespace Core\Foundation\Request;


interface RequestInterface
{
    public function all();

    public function get($key);

    public function getMethod();

    public function getPath();
}
