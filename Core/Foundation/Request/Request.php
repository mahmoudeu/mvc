<?php
/**
 * Created by PhpStorm.
 * User: macbookpro
 * Date: 16/03/2020
 * Time: 00:33
 */

namespace Core\Foundation\Request;


class Request implements RequestInterface
{
    private $params = [];
    private $method = [];
    private $path = [];

    public function __construct()
    {
        $this->params = $_REQUEST;
        $this->method = $_SERVER['REQUEST_METHOD'];
        $explodedPath = explode('?',$_SERVER["REQUEST_URI"]);
        $this->path = array_shift($explodedPath);
    }

    public function get($key)
    {
        return $this->params[$key] ?? null;
    }

    public function all()
    {
        return $this->params;
    }

    /**
     * @return array
     */
    public function getMethod()
    {
        return $this->method;
    }

    /**
     * @return array
     */
    public function getPath()
    {
        return $this->path;
    }

}
