<?php
/**
 * Created by PhpStorm.
 * User: macbookpro
 * Date: 16/03/2020
 * Time: 01:48
 */

namespace Core\Foundation\Config;


class Config
{
    /**
     * @var array
     */
    private $configs;

    public function __construct(array $configs)
    {
        $this->configs = $configs;
    }

    public function get($key)
    {
        $keys = explode('.', $key);
        $config = $this->configs;
        foreach ($keys as $key) {
            $config = $config[$key] ?? null;
        }

        return $config;
    }
}
