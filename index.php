<?php

ini_set('error_reporting', E_ALL);
ini_set('display_errors', 1);

require_once "vendor/autoload.php";

$appConfig = new \Core\Foundation\Application\AppConfig(
    __DIR__,
    __DIR__ . '/configs'
);

$app = new \Core\Foundation\Application\Application($appConfig);


$app->run();

$app->handleRequest();

